grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat)+ EOF!;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
     | PRT expr -> ^(PRT expr)
    | BLOCK_START NL -> BLOCK_START
    | BLOCK_END NL -> BLOCK_END
    | doWhileLoop NL -> doWhileLoop
    | whileLoop NL -> whileLoop
    | forLoop NL -> forLoop
    | NL ->
    ;

expr
    : addExpr
      ( CMP^ addExpr
      | NCMP^ addExpr
      )*
    ;
    
    
addExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;
    PRT
  : 'print'
  ;
    
doWhileLoop
    :DO^ expr WHILE! expr
    ;

whileLoop
    :WHILE^ expr DO! expr
    ;

forLoop
    :FOR^ expr DO! expr
    ;

VAR 
  :
  'var'
  ;

DO
  : 'do'
  ;
  
WHILE
  : 'while'
  ;

FOR
  : 'for'
  ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
		
	MOD
  : '%'
  ; 

POW
  : '^'
  ;
  
  BLOCK_START 
  : '{'
  ;
  

BLOCK_END 
  : '}'
  ;

CMP
  : '=='
  ;
  
NCMP
  :'!='
  ;
	
