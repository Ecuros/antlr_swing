tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje; separator=\" \n\"> 
start: <name;separator=\" \n\"> ";

decl    :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
        ;
        catch [RuntimeException ex] {errorID(ex,$i1);}
        

expr    : ^(PLUS  e1=expr e2=expr)  -> add(p1={$e1.st}, p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)  -> subtract(p1={$e1.st}, p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)  -> multiply(p1={$e1.st}, p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)  -> divide(p1={$e1.st}, p2={$e2.st})
        | ^(PODST i1=ID   e2=expr)  -> replace(symbol={$ID.text}, value={$e2.st})
        | ID                        -> id(symbol={$ID.text})
        | INT  {numer++;}           -> int(i={$INT.text},j={numer.toString()})
        | ^(CMP   e1=expr e2=expr)  -> equal(p1={$e1.st}, p2={$e2.st})
        | ^(NCMP   e1=expr e2=expr) -> different(p1={$e1.st}, p2={$e2.st})
        | ^(DO     e1=expr e2=expr) {numer++;}  -> do(exp1={$e1.st}, exp2={$e2.st}, n={numer.toString()})
        | ^(WHILE  e1=expr e2=expr) {numer++;}  -> whileLoop(exp1={$e1.st}, exp2={$e2.st}, n={numer.toString()})
        | ^(FOR    e1=expr e2=expr) {numer++;}  -> forLoop(exp1={$e1.st}, exp2={$e2.st}, n={numer.toString()})
    ;
    